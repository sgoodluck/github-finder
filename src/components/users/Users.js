import React, { Fragment, useContext } from 'react';

import GithubContext from 'context/github/githubContext';
import Spinner from 'components/layout/Spinner';
import UserItem from './UserItem';

const Users = () => {
	const githubContext = useContext(GithubContext);
	const { loading, users } = githubContext;

	return (
		<Fragment>
			{loading ? (
				<Spinner />
			) : (
				<div style={userStyle}>
					{users.map(user => (
						<UserItem key={user.id} user={user} />
					))}
				</div>
			)}
		</Fragment>
	);
};

const userStyle = {
	display: 'grid',
	gridTemplateColumns: 'repeat(3, 1fr)',
	gridGap: '1rem'
};

export default Users;
