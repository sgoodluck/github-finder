import {
	CLEAR_USERS,
	GET_REPOS,
	GET_USER,
	SEARCH_USERS,
	SET_LOADING
} from 'context/types';
import React, { useReducer } from 'react';

import GithubContext from './githubContext';
import GithubReducer from './githubReducer';
import axios from 'axios';

let githubClientId;
let githubClientSecret;

if (process.env.NODE_ENV !== 'production ') {
	githubClientSecret = process.env.REACT_APP_GITHUB_CLIENT_SECRET;
	githubClientId = process.env.REACT_APP_GITHUB_CLIENT_ID;
} else {
	githubClientSecret = process.env.GITHUB_CLIENT_SECRET;
	githubClientId = process.env.GITHUB_CLIENT_ID;
}

const GithubState = props => {
	// Global state for anything related to github
	const initialState = {
		users: [],
		user: {},
		repos: [],
		loading: false
	};

	const [state, dispatch] = useReducer(GithubReducer, initialState);

	// We call an action (i.e. calling github for data), then dispatch type to a
	// reducer which tells the app when to reload.

	// Search Users
	const searchUsers = async text => {
		setLoading();

		const res = await axios.get(
			`https://api.github.com/search/users?q=${text}&client_id=
			${githubClientId}
			&client_secret=${githubClientSecret}`
		);

		dispatch({
			type: SEARCH_USERS,
			payload: res.data.items
		});
	};

	// Get User
	const getUser = async username => {
		setLoading();

		const res = await axios.get(
			`https://api.github.com/users/${username}?client_id=
			${githubClientId}
			&client_secret=${githubClientSecret}`
		);

		dispatch({
			type: GET_USER,
			payload: res.data
		});
	};

	// Get Repos
	const getUserRepos = async username => {
		setLoading();

		const res = await axios.get(
			`https://api.github.com/users/${username}/repos?per_page=5&sort=created:asc&client_id=
			${githubClientId}
			&client_secret=${githubClientSecret}`
		);

		dispatch({
			type: GET_REPOS,
			payload: res.data
		});
	};

	// Clear Users
	const clearUsers = () => dispatch({ type: CLEAR_USERS });

	// Set Loading
	const setLoading = () => dispatch({ type: SET_LOADING });

	// Makes the data available to the entire app (via provider)
	return (
		<GithubContext.Provider
			value={{
				users: state.users,
				user: state.user,
				repos: state.repos,
				loading: state.loading,
				searchUsers,
				clearUsers,
				getUser,
				getUserRepos
			}}
		>
			{props.children}
		</GithubContext.Provider>
	);
};

export default GithubState;
